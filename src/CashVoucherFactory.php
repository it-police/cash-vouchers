<?php

namespace ITPolice\CashVouchers;

use ITPolice\CashVouchers\Vouchers\AtolHelper;
use ITPolice\CashVouchers\Vouchers\BifitHelper;
use ITPolice\CashVouchers\Vouchers\CheckOnlineHelper;
use ITPolice\CashVouchers\Vouchers\CloudKassir;
use ITPolice\CashVouchers\CashVoucher;

final class CashVoucherFactory
{
    /**
     * @param $type
     * @return CashVoucher
     */

    public static function factory($type)
    {
        if ($type == 'atol') {
            return new AtolHelper();
        } elseif ($type == 'checkonline') {
            return new CheckOnlineHelper();
        } elseif ($type == 'cloudkassir') {
            return new CloudKassir();
        } elseif ($type == 'bifit') {
            return new BifitHelper();
        }

        return null;
    }
}
