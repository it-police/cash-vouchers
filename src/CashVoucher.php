<?php


namespace ITPolice\CashVouchers;


use ITPolice\CashVouchers\Enum\Vates;

interface CashVoucher
{
    public function addItem($description, $price, $quantity = 1);
    public function addEmail($email);
    public function addPhone($phone);

    /**
     * @param $requestId
     * @return boolean
     */
    public function send($requestId);

    /**
     * @param $uuid
     * @return mixed (string 'success', string 'error', null)
     */
    public function getResponseStatus($uuid);
    public function getErrorDescription();

    public function setVat(Vates $vat);
    public function getVat();

    public function setIsAgent($bool);
}
