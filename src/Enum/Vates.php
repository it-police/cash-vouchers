<?php
namespace ITPolice\CashVouchers\Enum;

enum Vates: string {
    case WITHOUT = 'without';
    case VAT0 = "0";
    case VAT10 = "10";
    case VAT20 = "20";
    case VAT110 = "110";
    case VAT120 = "120";

    public static function fromValue(self $val): Vates
    {
        return $val;
    }
}
