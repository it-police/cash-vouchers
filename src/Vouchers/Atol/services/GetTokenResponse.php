<?php

namespace ITPolice\CashVouchers\Vouchers\Atol\services;

class GetTokenResponse extends BaseServiceResponse
{

	/** @var int */
	public $code;
	/** @var string */
	public $token;
}
