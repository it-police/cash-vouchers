<?php

namespace ITPolice\CashVouchers\Vouchers\Atol\clients;

use ITPolice\CashVouchers\Vouchers\Atol\services\BaseServiceRequest;

interface iClient
{

	/**
	 * Послать запрос
	 * @param BaseServiceRequest $service
	 * @return stdClass
	 */
	public function sendRequest(BaseServiceRequest $service);
}
