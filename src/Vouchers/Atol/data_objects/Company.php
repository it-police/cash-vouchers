<?php

namespace ITPolice\CashVouchers\Vouchers\Atol\data_objects;

use ITPolice\CashVouchers\Vouchers\Atol\handbooks\SnoTypes;

class Company extends BaseDataObject
{
	/** @var string */
	protected $email;
	/** @var string */
	protected $sno;
	/** @var int */
	protected $inn;
	/** @var string */
	protected $payment_address;

	public function __construct($email, SnoTypes $sno, $inn, $paymentAddress)
	{
		$this->email = (string)$email;
		$this->sno = $sno->getValue();
		$this->inn = (string)$inn;
		$this->payment_address = (string)$paymentAddress;
	}
}
