<?php

namespace ITPolice\CashVouchers\Vouchers\Atol\handbooks;

use MyCLabs\Enum\Enum;

class CorrectionTypes extends Enum
{
	const
		INSTRUCTION = 'instruction',
		SELF = 'self';
}
