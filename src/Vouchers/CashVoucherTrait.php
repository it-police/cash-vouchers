<?php


namespace ITPolice\CashVouchers\Vouchers;


use ITPolice\CashVouchers\Enum\Vates;

trait CashVoucherTrait
{

    public $requestId;
    protected $items = [];
    protected $customerEmail;
    protected $customerPhone;
    protected $errorDescription;
    protected Vates $vat;
    protected $isAgent = false;

    /**
     * @param string $description
     * @param float|integer $price
     * @param int $quantity
     */
    public function addItem($description, $price, $quantity = 1)
    {
        $this->items[] = [
            "quantity"    => $quantity,
            "price"       => $price,
            "description" => $description,
        ];
    }

    /**
     * @param $email
     */
    public function addEmail($email)
    {
        $this->customerEmail = $email;
    }

    /**
     * @param $phone
     */
    public function addPhone($phone)
    {
        $this->customerPhone = $phone;
    }

    /**
     * @return mixed
     */
    public function getErrorDescription() {
        return $this->errorDescription;
    }


    /**
     * @param Vates $vat
     * @return void
     */
    public function setVat(Vates $vat)
    {
        $this->vat = $vat;
    }

    public function setIsAgent($bool = true)
    {
        $this->isAgent = (boolean) $bool;
    }
}
