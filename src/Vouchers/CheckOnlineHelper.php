<?php

namespace ITPolice\CashVouchers\Vouchers;

use ITPolice\CashVouchers\CashVoucher;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use ITPolice\CashVouchers\Enum\Vates;

class CheckOnlineHelper implements CashVoucher
{
    use CashVoucherTrait;

    public $lastRequest;
    public $lastResponse;
    private $testUrl;
    private $url;

    public function __construct()
    {
        $this->testUrl = env('CHECKONLINE_TEST_URL', 'https://kkt4.chekonline.ru:4443/fr/api/v2');
        $this->url = env('CHECKONLINE_URL', 'https://kkt4.chekonline.ru/fr/api/v2');
    }

    public function send($requestId) {
        // отправляем запрос в сервис
        $res = $this->Complex($requestId);
        if (!$res) throw new \Exception("CheckOnline no result");

//        // генерируем чек в pdf
//        $cashVoucherFile = $this->toPdf();
//
//        // Оповещаем плательщика по email
//        $this->sendToEmail($cashVoucherFile);

        return $this->requestId;
    }

    /**
     * @param $requestId
     * @return bool
     * @throws \Exception
     */
    protected function Complex($requestId)
    {
        $this->requestId = $requestId;

        $url = (env('CHECKONLINE_TEST_MODE') ? $this->testUrl : $this->url) . '/Complex';

        $totalPrice = 0;
        $data = array(
            "Device"       => "auto",
            "FullResponse" => true,
            'RequestId'    => $requestId,
            "Lines"        => [],
            "TaxMode"      => (int) env('CHECKONLINE_TAX_MODE', 0),
            "PhoneOrEmail" => $this->customerEmail
        );
        foreach ($this->items as $item) {
            $d = [
                "Qty"          => $item['quantity'] * 1000,
                "Price"        => $item['price'] * 100,
                "PayAttribute" => (int) env('CHECKONLINE_PAY_ATTRIBUTE', 4),
                "LineAttribute" => (int) env('CHECKONLINE_LINE_ATTRIBUTE', 4),
                "TaxId"        => $this->getVat(),
                "Description"  => $item['description'],
            ];

            if ($this->isAgent) {
                $d['AgentModes'] = (int) env("CHECKONLINE_AGENT_MODE", 64);

                // new version
                if (env("CHECKONLINE_PROVIDER_NAME")) {
                    $d['ProviderData'] = [
                        "Name" => (string)env("CHECKONLINE_PROVIDER_NAME"),
                        "INN" => (string)env("CHECKONLINE_PROVIDER_INN"),
                        "Phone" => (string)env("CHECKONLINE_PROVIDER_PHONE"),
                    ];
                    $d['AgentData'] = [
                        "Operation" => (string)env("CHECKONLINE_AGENT_OPERATION"),
                        "Phone" => (string)env("CHECKONLINE_AGENT_PHONE"),
                    ];
                } // old version
                else {
                    $d['ProviderData'] = [
                        "Name" => env("CHECKONLINE_AGENT_NAME"),
                        "INN" => env("CHECKONLINE_AGENT_INN"),
                    ];
                }
            }

            $data['Lines'][] = $d;

            $totalPrice += $item['price'] * 100;
        }
        $data['NonCash'] = [$totalPrice];

        $certFileName = env('CHECKONLINE_TEST_MODE') ? 'chekonline-cert_test.pem' : 'chekonline-cert.pem';
        $keyFileName = env('CHECKONLINE_TEST_MODE') ? 'chekonline-key_test.pem' : 'chekonline-key.pem';

        $params = json_encode($data);
        $this->lastRequest = $params;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSLCERT, Storage::disk('local')->path('private/'.$certFileName));
        curl_setopt($curl, CURLOPT_SSLKEY, Storage::disk('local')->path('private/'.$keyFileName));

        $json_response = curl_exec($curl);

        $this->lastResponse = $json_response;
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $lastError = curl_error($curl);
        curl_close($curl);

        Log::debug(__CLASS__.':'.__METHOD__, [
            'params' => $params,
            'response' => $json_response
        ]);

        if ((int)$status != 200) {
            throw new \Exception($lastError. ' ' . $json_response);
        }

        $jsonObj = json_decode($json_response);
        if (!$jsonObj) return false;

        return true;
    }

    protected function toPdf()
    {
        $jsonObj = json_decode($this->lastResponse);
        $checkText = '';
        foreach ($jsonObj->Responses as $Response) {
            if($Response->Path == '/fr/api/v2/CloseDocument' && isset($Response->Response->Text)) {
                $checkText = $Response->Response->Text;
                break;
            }
        }
        if(!$checkText) {
            throw new \Exception("CheckOnline text not found");
        }
        $qrCode = $jsonObj->QR;
        $rows = array_map(function ($v) {
            return explode("\t", $v);
        }, explode("\n", $checkText));

        foreach ($rows as $k => $row) {
            if (count($row) == 1 && $row[0] == '') {
                unset($rows[$k]);
            }
        }

        $html = view('services.fce-online-check', compact('rows', 'qrCode'))->render();

        $pdfFilePathFull = Storage::disk('local')->path('private/checkonline/' . $this->requestId . '.pdf');
        \PDF::loadHTML($html)->save($pdfFilePathFull);

        return $pdfFilePathFull;

    }

    /**
     * @param $cashVoucherFile
     */
    protected function sendToEmail($cashVoucherFile) {
        $subject = "Чек об оплате на сайте " . \Config::get('app.name');
        $email = $this->customerEmail;

        Mail::raw('Чек об оплате',
            function ($m) use ($email, $cashVoucherFile, $subject) {
                $m->from(\Config::get('mail.from.address'), \Config::get('mail.from.name'));
                $m->to($email)->subject($subject);
                if ($cashVoucherFile) {
                    $m->attach($cashVoucherFile, [
                        'as'   => 'Чек об оплате.pdf',
                        'mime' => 'application/pdf',
                    ]);
                }
            }
        );

        @unlink($cashVoucherFile);
    }

    /**
     * @param $uuid
     * @return string|null
     */
    public function getResponseStatus($uuid) {
        $status = null;

        // todo
        $status = 'success';
        //$status = 'error';
        //$this->errorDescription = '';

        return $status;
    }

    public function getVat()
    {
        return match ($this->vat) {
            Vates::VAT0 => 3,
            Vates::VAT10 => 2,
            Vates::VAT20 => 1,
            Vates::VAT110 => 6,
            Vates::VAT120 => 5,
            Vates::WITHOUT => 4,
            default => (int) env('CHECKONLINE_TAX_ID', 4)
        };
    }
}
