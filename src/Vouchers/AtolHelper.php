<?php

namespace ITPolice\CashVouchers\Vouchers;

use ITPolice\CashVouchers\CashVoucher;
use ITPolice\CashVouchers\Vouchers\Atol\clients\PostClient;
use ITPolice\CashVouchers\Vouchers\Atol\data_objects\Client;
use ITPolice\CashVouchers\Vouchers\Atol\data_objects\Company;
use ITPolice\CashVouchers\Vouchers\Atol\data_objects\Item;
use ITPolice\CashVouchers\Vouchers\Atol\data_objects\Payment;
use ITPolice\CashVouchers\Vouchers\Atol\data_objects\Receipt;
use ITPolice\CashVouchers\Vouchers\Atol\data_objects\Service;
use ITPolice\CashVouchers\Vouchers\Atol\data_objects\Vat;
use ITPolice\CashVouchers\Vouchers\Atol\handbooks\PaymentMethods;
use ITPolice\CashVouchers\Vouchers\Atol\handbooks\PaymentObjects;
use ITPolice\CashVouchers\Vouchers\Atol\handbooks\PaymentTypes;
use ITPolice\CashVouchers\Vouchers\Atol\handbooks\ReceiptOperationTypes;
use ITPolice\CashVouchers\Vouchers\Atol\handbooks\SnoTypes;
use ITPolice\CashVouchers\Vouchers\Atol\handbooks\Vates;
use ITPolice\CashVouchers\Vouchers\Atol\SdkException;
use ITPolice\CashVouchers\Vouchers\Atol\services\CreateReceiptRequest;
use ITPolice\CashVouchers\Vouchers\Atol\services\CreateReceiptResponse;
use ITPolice\CashVouchers\Vouchers\Atol\services\GetStatusRequest;
use ITPolice\CashVouchers\Vouchers\Atol\services\GetStatusResponse;
use ITPolice\CashVouchers\Vouchers\Atol\services\GetTokenRequest;
use ITPolice\CashVouchers\Vouchers\Atol\services\GetTokenResponse;
use \ITPolice\CashVouchers\Enum\Vates as EnumVates;
class AtolHelper implements CashVoucher
{
    use CashVoucherTrait;

    protected $demoMode;
    protected $enableLogging;
    protected $login;
    protected $password;
    protected $groupCode;
    protected $inn;
    protected $paymentAddress;
    protected $email;
    protected $snoType;
//    protected $vat;
    protected $callbackUrl;
    private $client;

    public function __construct()
    {
        $this->demoMode = (bool) env('ATOL_TEST_MODE');
        $this->enableLogging = (bool) env('ATOL_LOG_REQUESTS');
        $this->login = $this->demoMode ? env('ATOL_TEST_LOGIN') : env('ATOL_LOGIN');
        $this->password = $this->demoMode ? env('ATOL_TEST_PASSWORD') : env('ATOL_PASSWORD');
        $this->groupCode = $this->demoMode ? env('ATOL_TEST_GROUP_CODE') : env('ATOL_GROUP_CODE');
        $this->inn = $this->demoMode ? env('ATOL_TEST_INN') : env('ATOL_INN');
        $this->paymentAddress = $this->demoMode ? env('ATOL_TEST_PAYMENT_ADDRESS') : env('ATOL_PAYMENT_ADDRESS');
        $this->email = $this->demoMode ? env('ATOL_TEST_EMAIL') : env('ATOL_EMAIL');
        $this->snoType = env('ATOL_SNO_TYPE', SnoTypes::OSN);
        //$this->vat = env('ATOL_VAT', Vates::VAT120);
        $this->callbackUrl = env('ATOL_CALLBACK_URL') ? url(env('ATOL_CALLBACK_URL')) : null;

        $this->client = new PostClient();
        if($this->enableLogging) $this->client->addLogger(\Log::getLogger());
    }

    /**
     * @param $requestId
     * @return bool
     * @throws \Exception
     */
    public function send($requestId)
    {
        $this->requestId = $requestId;

        $tokenRequest = new GetTokenRequest($this->login, $this->password);
        if ($this->demoMode) $tokenRequest->setDemoMode();
        $tokenResponse = new GetTokenResponse($this->client->sendRequest($tokenRequest));


        if (!$tokenResponse->isValid())
            throw new \Exception($tokenResponse->getErrorDescription());

        $items = $this->createItems();
        $payment = $this->createPayment($items);
        $customer = $this->createCustomer();
        $company = $this->createCompany();
        $receipt = new Receipt($customer, $company, $items, $payment, new ReceiptOperationTypes(ReceiptOperationTypes::SELL));
        $externalId = time();
        $createReceiptRequest = new CreateReceiptRequest(
            $tokenResponse->token,
            $this->groupCode,
            $externalId,
            $receipt
        );
        if ($this->demoMode) $createReceiptRequest->setDemoMode();
        if ($this->callbackUrl) $createReceiptRequest->addService(new Service($this->callbackUrl));
        $createReceiptResponse = new CreateReceiptResponse($this->client->sendRequest($createReceiptRequest));

        if (!$createReceiptResponse->isValid())
            throw new \Exception($createReceiptResponse->getErrorDescription());

        return $createReceiptResponse->uuid;
    }

    /**
     * @return Item[]
     */
    private function createItems()
    {
        $items = [];
        foreach ($this->items as $item) {
            $vat = $this->createVat();
            $item = new Item(
                $item['description'],
                $item['price'],
                $item['quantity'],
                $vat
            );
            //$agentInfo = $this->createAgentInfo();
            //$item->addAgentInfo($agentInfo);
            //$item->getPositionSum(20);
            $item->addMeasurementUnit('шт.');
            $item->addPaymentMethod(new PaymentMethods(PaymentMethods::FULL_PAYMENT));
            $item->addPaymentObject(new PaymentObjects(PaymentObjects::SERVICE));
            //$item->addUserData('Test user data');
            $items[] = $item;
        }
        return $items;
    }

    /**
     * @return Vat
     */
    private function createVat()
    {
        $vat = new Vat(new Vates($this->getVat()));
        //$vat->addSum(2);
        return $vat;
    }

    /**
     * @param  $items Item[]
     * @return Payment
     */
    private function createPayment($items)
    {
        $payment = new Payment(
            new PaymentTypes(PaymentTypes::ELECTRON),
            array_sum(array_map(function ($i) {
                return $i->getPositionSum();
            }, $items))
        );
        return $payment;
    }

    /**
     * @return Client
     */
    private function createCustomer()
    {
        $customer = new Client();
        if ($this->customerEmail) $customer->addEmail($this->customerEmail);
        if ($this->customerPhone) $customer->addPhone($this->customerPhone);
        return $customer;
    }

    /**
     * @return Company
     */
    private function createCompany()
    {
        $company = new Company(
            $this->email,
            new SnoTypes($this->snoType),
            $this->inn,
            $this->paymentAddress
        );
        return $company;
    }

    /**
     * @param string $uuid
     * @param int $seconds
     * @return bool|GetStatusResponse
     * @throws SdkException
     */
    public function checkReceiptStatus($uuid, $seconds = 10)
    {
        $tokenRequest = new GetTokenRequest($this->login, $this->password);
        if ($this->demoMode) $tokenRequest->setDemoMode();
        $tokenResponse = new GetTokenResponse($this->client->sendRequest($tokenRequest));

        $getStatusRequest = new GetStatusRequest($this->groupCode, $uuid, $tokenResponse->token);
        if ($this->demoMode) $getStatusRequest->setDemoMode();

        for ($second = 0; $second <= $seconds; $second++) {
            $getStatusResponse = new GetStatusResponse($this->client->sendRequest($getStatusRequest));
            if ($getStatusResponse->isReceiptReady()) {
                return $getStatusResponse;
            } else {
                $second++;
            }
            sleep(1);
        }
        return false;
    }

    /**
     * @param $uuid
     * @return string|null
     * @throws SdkException
     */
    public function getResponseStatus($uuid) {
        $tokenRequest = new GetTokenRequest($this->login, $this->password);
        if ($this->demoMode) $tokenRequest->setDemoMode();
        $tokenResponse = new GetTokenResponse($this->client->sendRequest($tokenRequest));

        $getStatusRequest = new GetStatusRequest($this->groupCode, $uuid, $tokenResponse->token);
        if ($this->demoMode) $getStatusRequest->setDemoMode();
        $getStatusResponse = new GetStatusResponse($this->client->sendRequest($getStatusRequest));

        $status = null;
        if($getStatusResponse->isValid()) {
            $status = 'success';
        } else if($getStatusResponse->status != 'wait'){
            $status = 'error';
            $this->errorDescription = $getStatusResponse->getErrorDescription();
        }

        return $status;
    }

    public function getVat()
    {
        return match ($this->vat) {
            EnumVates::VAT0 => "vat0",
            EnumVates::VAT10 => "vat10",
            EnumVates::VAT20 => "vat20",
            EnumVates::VAT110 => "vat110",
            EnumVates::VAT120 => "vat120",
            EnumVates::WITHOUT => "none",
            default => env('ATOL_VAT', "none")
        };
    }
}
