<?php

namespace ITPolice\CashVouchers\Vouchers;

use ITPolice\CashVouchers\CashVoucher;
use Illuminate\Support\Facades\Log;
use ITPolice\CashVouchers\Enum\Vates;
use function Clue\StreamFilter\fun;

class CloudKassir implements CashVoucher
{
    use CashVoucherTrait;

    protected $publicId;
    protected $apiSecret;
    protected $inn;
    protected $taxationSystem;
//    protected $vat;
    private $url = 'https://api.cloudpayments.ru';

    public function __construct()
    {
        $this->publicId = env('CLOUDKASSIR_PUBLIC_ID');
        $this->apiSecret = env('CLOUDKASSIR_API_SECRET');
        $this->inn = env('CLOUDKASSIR_COMPANY_INN');
        $this->taxationSystem = env('CLOUDKASSIR_TAXATION_SYSTEM');
//        $this->vat = env('CLOUDKASSIR_VAT');
    }

    /**
     * @param $requestId
     * @return bool
     * @throws \Exception
     */
    public function send($requestId)
    {
        $this->requestId = $requestId;

        $items = [];
        foreach ($this->items as $item) {
            $item = [
                "Label" => $item['description'],
                "Price" => $item['price'],
                "Quantity" => $item['quantity'],
                "Amount" => $item['price'] * $item['quantity'],
                "Vat" => $this->getVat(),
            ];
            if ($this->isAgent) {
                $item['AgentSign'] = 6;
                $item['PurveyorData'] = [
                    "Phone" => env("CLOUDKASSIR_AGENT_PHONE", null),
                    "Name" => env("CLOUDKASSIR_AGENT_NAME"),
                    "Inn" => env("CLOUDKASSIR_AGENT_INN"),
                ];
                $item['AgentData'] = [
                    "AgentOperationName" => null,
                    "PaymentAgentPhone" => null,
                    "PaymentReceiverOperatorPhone" => null,
                    "TransferOperatorPhone" => null,
                    "TransferOperatorName" => null,
                    "TransferOperatorAddress" => null,
                    "TransferOperatorInn" => null,
                ];
            }
            $items[] = $item;
        }

        $params = [
            'InvoiceId' => $this->requestId,
            'Inn' => $this->inn,
            'Type' => 'Income',
            'CustomerReceipt' => [
                'Items' => $items,
                'TaxationSystem' => $this->taxationSystem,
                'Amounts' => [
                    'Electronic' => array_sum(array_map(function ($v) {
                        return $v['Amount'];
                    }, $items))
                ]
            ],
        ];


        if ($this->customerEmail) $params['CustomerReceipt']['Email'] = $this->customerEmail;
        if ($this->customerPhone) $params['CustomerReceipt']['Phone'] = $this->customerPhone;

        $res = $this->sendRequest('/kkt/receipt', 'POST', $params);
        if ($res->Success == true) {
            return $res->Model->Id;
        } else {
            throw new \Exception($res->Message);
        }
    }

    public function sendRequest($action, $method = 'GET', $params = [], $headers = [], $url = false)
    {
        $url = ($url !== false ? $url : $this->url) . $action;
        if ($method == 'GET' && !empty($params)) {
            $url .= '?' . urldecode(http_build_query($params));
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->publicId . ":" . $this->apiSecret);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_replace([
            'Content-Type: application/json',
        ], $headers));
        if ($method == 'POST') {
            $postParams = json_encode($params, JSON_UNESCAPED_UNICODE);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
        } elseif ($method == 'PUT') {
            $postParams = json_encode($params, JSON_UNESCAPED_UNICODE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
        } elseif ($method == 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $responseObject = json_decode($response);

        $responseLog = $response;
        if (!is_object($responseObject) || $httpCode != 200) {
            try {
                if (is_object($responseObject)) {
                    $responseLog = (object)[
                        'code' => $responseObject->code,
                        'message' => $responseObject->message
                    ];
                } else {
                    $responseLog = (object)[
                        'code' => $httpCode,
                        'message' => 'Неизвестная ошибка'
                    ];
                }
                throw new \Exception(__CLASS__);
            } catch (\Exception $e) {
            }

            Log::error(__CLASS__ . ' response error', [
                'url' => $url,
                'method' => $method,
                'params' => ($method == 'POST' || $method == 'PUT') ? $postParams : false,
                'headers' => $headers,
                'response' => $responseLog
            ]);

            throw new \Exception($response);
        }

        if (env('CLOUDKASSIR_LOG_REQUESTS')) {
            Log::debug(__CLASS__ . ' response log', [
                'url' => $url,
                'method' => $method,
                'params' => ($method == 'POST' || $method == 'PUT') ? $postParams : false,
                'headers' => $headers,
                'response' => $responseLog
            ]);
        }

        return $responseObject;
    }

    public function getResponseStatus($uuid)
    {
        $status = null;

        $params = [
            'Id' => $uuid
        ];

        $res = $this->sendRequest('/kkt/receipt/status/get', 'POST', $params);
        if (@$res->Success == true && @$res->Model == 'Processed') {
            $status = 'success';
        } else if (@$res->Success == true && @$res->Model != 'Queued') {
            $status = 'error';
            $this->errorDescription = $res->Message;
        }

        return $status;
    }

    public function getVat()
    {
        return match ($this->vat) {
            Vates::VAT0 => 0,
            Vates::VAT10 => 10,
            Vates::VAT20 => 20,
            Vates::VAT110 => 110,
            Vates::VAT120 => 120,
            Vates::WITHOUT => null,
            default => env('CLOUDKASSIR_VAT', null)
        };
    }
}
