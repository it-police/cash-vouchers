<?php

namespace ITPolice\CashVouchers\Vouchers;

use ITPolice\CashVouchers\CashVoucher;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use ITPolice\CashVouchers\Enum\Vates;

class BifitHelper implements CashVoucher
{
    use CashVoucherTrait;

    private $testUrl = 'https://fp-test.bifit.com/processing-api';
    private $url = 'https://fp.bifit.com/processing-api';

    protected $token;
    protected $pin;
    protected $calculationMethod;
    //protected $vat;
    protected $paymentSubject;
    protected $docType;
    protected $taxSystem;


    private $access_token;

    public function __construct()
    {
        $this->token = env('BIFIT_TOKEN');
        $this->pin = env('BIFIT_PIN');
        //$this->vat = env('BIFIT_VAT', 'WITHOUT_VAT');
        $this->paymentSubject = env('BIFIT_PAYMENT_SUBJECT', 'SERVICE');
        $this->calculationMethod = env('BIFIT_CALCULATION_METHOD', 'FULL_PAY');
        $this->docType = env('BIFIT_DOC_TYPE', 'SALE');
        $this->taxSystem = env('BIFIT_TAX_SYSTEM', 'SIMPLIFIED');
    }

    /**
     * @param $requestId
     * @return bool
     * @throws \Exception
     */
    public function send($requestId)
    {
        $this->requestId = $requestId;
        if (!$this->getToken()) {
            throw new \Exception("Ошибка получения токена!");
        }
        $res = $this->createReceipt($requestId);

        if ($res['success'] == true) {
            return $res['id'];
        } else {
            throw new \Exception("Ошибка создания чека!");
        }
    }

    protected function getToken()
    {
        $params = [
            'token' => $this->token,
            'client_id' => 'processing-connector-token',
            'client_secret' => 'processing-connector-token',
            'grant_type' => 'token',
        ];
        $headers = array(
            'Content-type: application/x-www-form-urlencoded'
        );
        $postParams = http_build_query($params, '', '&');
        $res = $this->sendRequest('/oauth/token', 'POST', $headers, $postParams);

        if ($res->access_token) {
            $this->access_token = $res->access_token;
        } else {
            return false;
        }

        return true;
    }

    protected function createReceipt($requestId)
    {
        $items = [];
        foreach ($this->items as $item) {
            $items[] = [
                "calculationMethod" => $this->calculationMethod,
                "paymentSubject" => $this->paymentSubject,
                "name" => $item['description'],
                "price" => $item['price'],
                "quantity" => $item['quantity'],
                "vat" => $this->getVat(),
                "total" => $item['price'] * $item['quantity']
            ];
        }

        $total = array_sum(array_map(function ($v) {
            return $v['total'];
        }, $items));

        $params = [
            "type" => $this->docType,
            "taxSystem" => $this->taxSystem,
            "cashier" => [
                "name" => env('BIFIT_CASHIER_NAME', "Иванов И.И.")
            ],
            "items" => $items,
            "total" => $total,
            "payments" => [
                "CARD" => $total,
            ]
        ];

        if ($this->customerEmail) {
            $params['client']['address'] = $this->customerEmail;
        } else if ($this->customerPhone) {
            $params['client']['address'] = $this->customerPhone;
        }

        $idempotency_key = hash("sha256", $requestId . '/'. $this->pin .'/' .
            $total . '/' . Carbon::now()->format('d-m-Y H:i'));

        $headers = array(
            'Content-type: application/json',
            'Authorization: Bearer' . ' ' . $this->access_token,
            'Idempotency-Key: ' . $idempotency_key
        );

        $receipt_id = $this->sendRequest('/protected/documents/registration/receipts', 'POST', $headers, json_encode($params));

        $result = [
            'success' => true,
            'id' => $receipt_id
        ];

        return $result;
    }

    protected function sendRequest($action, $method = 'GET', $headers = [], $params = [], $url = false)
    {
        $url = (env('BIFIT_TEST_MODE') ? $this->testUrl : $this->url) . $action;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $responseObject = json_decode($response);
        $responseLog = $response;

        if ($httpCode != 200) {
            try {
                if (is_object($responseObject)) {
                    $responseLog = (object)[
                        'code' => $responseObject->code,
                        'message' => $responseObject->message
                    ];
                } else {
                    $responseLog = (object)[
                        'code' => $httpCode,
                        'message' => 'Неизвестная ошибка'
                    ];
                }
                throw new \Exception(__CLASS__);
            } catch (\Exception $e) {
            }
            Log::error(__CLASS__ . ' response error', [
                'url' => $url,
                'method' => $method,
                'params' => $params,
                'headers' => $headers,
                'response' => $responseLog
            ]);
            throw new \Exception($response);
        }

        if (env('BIFIT_LOG_REQUESTS')) {
            Log::debug(__CLASS__ . ' response log', [
                'url' => $url,
                'method' => $method,
                'params' => $params,
                'headers' => $headers,
                'response' => $responseLog
            ]);
        }

        return $responseObject;
    }

    public function getResponseStatus($uuid)
    {
        return 'success';
    }

    public function getVat()
    {
        return match ($this->vat) {
            Vates::VAT0 => "VAT_0",
            Vates::VAT10 => "VAT_10",
            Vates::VAT20 => "VAT_20",
            Vates::VAT110 => "VAT_110",
            Vates::VAT120 => "VAT_120",
            Vates::WITHOUT => "WITHOUT_VAT",
            default => env('BIFIT_VAT', 'WITHOUT_VAT')
        };
    }
}
